package de.dynamaze.inventorysystem.Controller;



import de.dynamaze.inventorysystem.Model.Leisure;
import de.dynamaze.inventorysystem.Repository.leisureRepository;
import de.dynamaze.inventorysystem.Repository.providerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("leisure/api/v1")
public class leisureController {

    @Autowired
    public leisureRepository leisureRepo;
    @Autowired
    public providerRepository providerRepository;

    @PostMapping("/leisures")
    public Leisure leisure (@RequestBody Leisure l){
        return leisureRepo.save(l);
    }

    @GetMapping("/providers")
    public List<Leisure> findAllLeisure(Model model){
        model.addAttribute("Provider", leisureRepo.findAll());
        return leisureRepo.findAll();
    }

    @GetMapping("/singleProvider/{id}")
    public Leisure singleLeisure(@PathVariable(value= "id") long id){
        return leisureRepo.findById(id);
    }

}
