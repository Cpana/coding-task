package de.dynamaze.inventorysystem.Repository;


import de.dynamaze.inventorysystem.Model.Leisure;
import org.springframework.data.annotation.Id;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface leisureRepository extends CrudRepository<Leisure, Id> {
    List<Leisure> findAll();
    Leisure findById(long id);
}
