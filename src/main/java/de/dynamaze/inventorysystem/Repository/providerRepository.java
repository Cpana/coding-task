package de.dynamaze.inventorysystem.Repository;

import de.dynamaze.inventorysystem.Model.Leisure;
import de.dynamaze.inventorysystem.Model.Provider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Id;
import java.util.List;

@Repository
public interface providerRepository extends CrudRepository<Provider, Id> {
    List<Provider> findAll();
}
