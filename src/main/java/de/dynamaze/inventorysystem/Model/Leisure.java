package de.dynamaze.inventorysystem.Model;

import javax.persistence.*;

@Entity
public class Leisure {
    @Id
    private Long id;
    private String activity;
    private String hint;
    private String description;
    private String headline;


    //Init Constructor
    public Leisure() {
    }

    public Leisure(Long id, String activity, String hint, String description, String headline) {
        this.id = id;
        this.activity = activity;
        this.hint = hint;
        this.description = description;
        this.headline = headline;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }
}

