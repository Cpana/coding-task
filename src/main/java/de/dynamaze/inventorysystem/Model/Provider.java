package de.dynamaze.inventorysystem.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Provider {


    @Id
    private long ID;

    @ManyToOne
    @JoinColumn(name="Leisure")
    Leisure leisure;
    private String name;
    private String address;
    private String website;

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public Leisure getLeisure() {
        return leisure;
    }

    public void setLeisure(Leisure leisure) {
        this.leisure = leisure;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }




    public Provider(long ID, Leisure leisure, String name, String address, String website) {
        this.ID = ID;
        this.leisure = leisure;
        this.name = name;
        this.address = address;
        this.website = website;
    }



}
